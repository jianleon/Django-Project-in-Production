# Poner un proyecto Django en producción

Guía muy, muy básica para poner en producción un proyecto Django usando Debian, Nginx y UWSGI.

Se aceptan `merge request`, siéntete libre de aportar :) 



# Siempre usar un usuario diferente al root

```shell
~# adduser dev

~# adduser dev sudo

~# su dev

$ sudo apt update

$ sudo apt upgrade

```
# Instalar pip
```shell
$ sudo apt install python2

$ curl https://bootstrap.pypa.io/get-pip.py --output get-pip.py

$ sudo python2 get-pip.py
```

# Instalar librerías de desarrollo
```shell
$ sudo apt install -y build-essential libssl-dev libffi-dev python3-dev

$ sudo apt install git postgresql-12 postgresql-contrib-12 libpq-dev

```


# Instalar código
```shell
$ mkdir Projects

$ cd Projects/

$ git clone --recursive https://gitlab.com/mi-proyecto.git

$ cd mi-proyecto/

$ sudo pip install virtualenvwrapper

$ mkdir -p ~/.envs

$ vim ~/.bashrc
```

Poner estas lineas al final del archivo:

```
export WORKON_HOME=~/.envs
source /usr/local/bin/virtualenvwrapper.sh
```
    
Recargar el entorno:
```shell
$ source ~/.bashrc
```

## Instalar entorno python

```shell
$ mkvirtualenv miproyecto -p python3
```


# Configurar postgres:

En la sección `# "local" is for Unix domain socket connections only` cambiar: `local all all peer` por `local all all md5`:

```shell
$ sudo vim /etc/postgresql/12/main/pg_hba.conf
```

Reiniciar postgres:
```shell
$ sudo service postgresql restart
```

Crear usuario y base de datos:

```shell
$ sudo su - postgres

$ psql

postgres=# CREATE ROLE myuser CREATEDB CREATEROLE LOGIN PASSWORD 'database_pass';

postgres=# \q

$ logout  # Dejar de ser root
```
Crear base de datos
```shell
$ createdb miproyecto_web -U myuser
```

# Configurar datos de acceso

Configurar el archivo mi-proyecto/settings/local_settings.py

```shell
cp mi-proyecto/local_settings.py.template mi-proyecto/local_settings.py & vim mi-proyecto/local_settings.py
```

## Instalar dependencias

```shell
$ pip install -r requirements.txt
$ pip install uwsgi
```

### Aplicar migraciones

```shell
$ python manage.py migrate
```

Crear un super usuario:

```shell
$ python manage.py createsuperuser

$ python manage.py collectstatic
```

# Instalar NGINX

```shell
$ sudo apt install nginx

$ sudo vim /etc/nginx/sites-available/miproyecto.conf
```

Agregar:


    upstream django_miproyecto {
        server unix:///home/dev/Projects/mi-proyecto/sock-miproyecto.sock;
    }

    server {
        listen      80; # puerto por el que escucha la aplicación
        server_name miproyecto.com www.miproyecto.com; # nombre o ip del servidor
        charset     utf-8;

        # tamaño maximo de subida
        client_max_body_size 75M;

        location = /robots.txt {
            alias /home/dev/.statics/miproyecto/robots.txt;
        }

        # Ruta de la carpeta media
        location /media  {
            alias /home/dev/Projects/mi-proyecto/public/media;
        }

        # Ruta de la carpeta static
        location /static {
            alias /home/dev/.statics/miproyecto; # reemplace por su ruta a media
        }

        # Configuración de las peticiones que no son resueltas con las
        # reglas anteriores. Estas son reenviadas al servidor django
        location / {
            include     uwsgi_params;
            uwsgi_pass  django_miproyecto;
            uwsgi_read_timeout 300;
        }
    }

# Configuraciones en el proyecto Django


```shell
STATIC_ROOT = "/home/dev/.statics/miproyecto"
```

# Enlazamiento de nginx de sites available a sites enabled

```shell
$ touch /home/dev/Projects/mi-proyecto/sock-miproyecto.sock
$ cd  /etc/nginx/
$ sudo ln -s /etc/nginx/sites-available/miproyecto.conf sites-enabled/
$ sudo service nginx restart
```

# Instalar supervisor

```shell
$ sudo apt install supervisor
$ cd ~/Projects/mi-proyecto
$ mkdir logs
$ touch logs/supervisor_out.log logs/supervisor_err.log
```

Agregar las siguientes lineas en el siguiente archivo:

```shell
$ sudo vim /etc/supervisor/conf.d/supervisor-miproyecto.conf
```

lineas:

    [program:miproyecto]

    directory=/home/dev/Projects/mi-proyecto/
    command=/home/dev/.envs/miproyecto/bin/uwsgi --socket sock-miproyecto.sock --wsgi-file mi-proyecto/wsgi.py --chmod-socket=666 --harakiri=300

    autostart=true
    autorestart=true

    user=dev

    stdout_logfile=/home/dev/Projects/mi-proyecto/logs/supervisor_out.log
    stderr_logfile=/home/dev/Projects/mi-proyecto/logs/supervisor_err.log

    redirect_stderr=true
    exitcodes=0,1


Recargar supervisor:

```shell
$ sudo supervisorctl reread
$ sudo supervisorctl update
```

# Monitorear la web

Instalar htop:

```shell
$ sudo apt install htop
```

Ver estado de supervisor

```shell
$ sudo supervisorctl status
```

Reiniciar:

```shell
$ sudo supervisorctl restart miproyecto
```

Detener:

```shell
$ sudo supervisorctl stop miproyecto
```
Iniciar:

```shell
$ sudo supervisorctl start miproyecto
```

## Ver logs
```shell
$ tailf -n 100 /home/dev/Projects/mi-proyecto/logs/supervisor_out.log

$ tailf -n 100 /home/dev/Projects/mi-proyecto/logs/supervisor_err.log
```
